import datetime
import unittest
from cores.facebook import FbArticleEntity
from repositories import facebook_repository


class TestToDao(unittest.TestCase):

    def test(self):
        now = datetime.datetime.now()
        fb_article_entity = FbArticleEntity(content='content',
                                            page_url='page_url',
                                            full_article_url='full_article_url',
                                            privacy='privacy',
                                            time=now)

        article_entity = facebook_repository.to_dao(fb_article_entity)

        self.assertEqual(article_entity.content, 'content')
        self.assertEqual(article_entity.page_url, 'page_url')
        self.assertEqual(article_entity.full_article_url, 'full_article_url')
        self.assertEqual(article_entity.privacy, 'privacy')
        self.assertEqual(article_entity.time, now)

    def test_none_emotion(self):
        article_entity = fb_article_entity = FbArticleEntity(emotion=None)
        self.assertEqual(article_entity.emotion['like'], 0)
        self.assertEqual(article_entity.emotion['haha'], 0)
        self.assertEqual(article_entity.emotion['love'], 0)
        self.assertEqual(article_entity.emotion['sad'], 0)
        self.assertEqual(article_entity.emotion['angry'], 0)
        self.assertEqual(article_entity.emotion['wow'], 0)

