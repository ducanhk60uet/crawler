from cores.facebook import FbCrawler, FbArticleEntity
from repositories import facebook_repository
from repositories.es_context import init_es_context
from dotenv import load_dotenv
load_dotenv()
import os


if __name__ == '__main__':

    username = os.environ['FB_USERNAME']
    password = os.environ['FB_PASSWORD']

    print(username, password)
    init_es_context()

    with FbCrawler() as fb_crawler:
        fb_crawler.login(username, password)
        article_entities = fb_crawler.get_articles('https://mbasic.facebook.com/thanhtunguet.info')
        print(facebook_repository.add(article_entities))
