from typing import List
from elasticsearch.helpers import bulk
from cores.facebook import FbArticleEntity
from repositories.es_context import Article, es_client


def to_dao(article_entity: FbArticleEntity) -> Article:
    article = Article()
    article.content = article_entity.content
    article.page_url = article_entity.page_url
    article.full_article_url = article_entity.full_article_url
    article.privacy = article_entity.privacy
    article.time = article_entity.time
    article.emotion = article_entity.emotion

    article.like = article_entity.emotion['like']
    article.haha = article_entity.emotion['haha']
    article.sad = article_entity.emotion['sad']
    article.angry = article_entity.emotion['angry']
    article.love = article_entity.emotion['love']
    article.wow = article_entity.emotion['wow']
    return article


def add(article_entities: List[FbArticleEntity]):
    articles = [to_dao(t) for t in article_entities]
    dicts = [a.to_dict(True) for a in articles]
    response = bulk(es_client, dicts)
    return response

