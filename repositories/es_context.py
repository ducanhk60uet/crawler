from elasticsearch_dsl import Document, Text, Date, Integer, Nested, Keyword, Long
from elasticsearch_dsl.connections import connections

es_client = connections.create_connection(
    hosts=['https://d70a9d941a114e22a4ef896543887738.us-east-1.aws.found.io:9243'],
    http_auth=('elastic', 'qTDBSsq41LQUBFoxsVPHh4rU'),
    maxsize=25
)


class Article(Document):
    content = Text(analyzer='standard')
    page_url = Keyword()
    full_article_url = Keyword()
    privacy = Keyword()
    time = Date()
    emotion = Nested()

    like = Long()
    haha = Long()
    sad = Long()
    angry = Long()
    love = Long()
    wow = Long()

    class Index:
        name = 'crawler-article-v3'


class Friend(Document):
    name = Text(type='text')
    url = Keyword()

    class Index:
        name = 'crawler-friend-v3'


def init_es_context():
    Article.init()
    Friend.init()
    article = Article(content='Hello, world', page_url='ducanhk60uet@gmail.com')
    article.save()


if __name__ == '__main__':
    init_es_context()
