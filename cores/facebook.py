import re
from datetime import datetime, timedelta
from typing import List
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from webdriver_manager.chrome import ChromeDriverManager
from cores.base import BaseCrawler
from cores.exception import LastPageException, ReturnException

emotion_dict = {
    'Thích': 'like',
    'Haha': 'haha',
    'Buồn': 'sad',
    'Phẫn nộ': 'angry',
    'Yêu thích': 'love',
    'Wow': 'wow'
}


class FbArticleEntity:

    def __init__(self, content=None, emotion=None, page_url=None, full_article_url=None, privacy=None, time=None):
        self.content = content
        self.emotion = emotion
        self.page_url = page_url
        self.full_article_url = full_article_url
        self.privacy = privacy
        self.time = time

        if not self.emotion:
            self.emotion = {name: 0 for name in emotion_dict.values()}

    def __repr__(self):
        return ""


class FbFriendEntity:

    def __init__(self, name=None, url=None):
        self.name = name
        self.url = url

    def to_dao(self):
        pass

    @staticmethod
    def from_dao():
        pass

    def __repr__(self):
        return str((self.name, self.url))


class FbCrawler(BaseCrawler):

    login_url = 'https://mbasic.facebook.com/login/?ref=dbl&fl'
    friend_url = 'https://mbasic.facebook.com/me/friends'

    def __init__(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_experimental_option("detach", True)

        # init selenium driver
        self.driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)

    def __exit__(self, exc_type, exc_value, traceback):
        # self.driver.quit()
        pass

    def detect_article_type(self):
        pass

    def get_full_article(self, url) -> (str, dict):
        content = emotion = None
        self.driver.execute_script(f'window.open("{url}", "__blank__");')
        self.driver.switch_to.window(self.driver.window_handles[1])

        try:
            # get full content
            content = self.driver.find_element_by_xpath("(//div[@data-ft])[1]/div[1]/*[2]").get_attribute('innerText')
        except NoSuchElementException:
            pass

        try:
            # get emotions
            emotion_url = self.driver.find_element_by_xpath("//div[contains(@id,'sentence_')]/a").get_attribute('href')
            self.driver.get(emotion_url)
        except NoSuchElementException:
            pass

        try:
            # init emotion dictionary
            emotion = {name: 0 for name in emotion_dict.values()}
            emotion_elements = self.driver.find_elements_by_xpath('//div[@class="y"]/a')
            for emotion_element in emotion_elements:
                try:
                    emotion_name = emotion_element.find_element_by_xpath('./img').get_attribute('alt')
                    emotion_count = int(emotion_element.find_element_by_xpath('./span').get_attribute('innerText'))
                    emotion[emotion_dict[emotion_name]] = emotion_count
                except NoSuchElementException:
                    pass
        except NoSuchElementException:
            pass

        self.driver.close()
        self.driver.switch_to.window(self.driver.window_handles[0])
        return content, emotion

    def get_friends(self) -> List[FbFriendEntity]:
        self.driver.get(FbCrawler.friend_url)
        friend_entities = []

        while True:
            friend_elements = self.driver.find_elements_by_xpath('//table[@role="presentation" and @class="m"]')[1:-1]

            for friend_element in friend_elements:
                friend_element = friend_element.find_element_by_tag_name('a')
                name = friend_element.text
                url = friend_element.get_attribute('href')
                friend_entities.append(FbFriendEntity(name=name, url=url))

            try:
                # next page
                self.driver.find_element_by_id('m_more_friends').find_element_by_tag_name('span').click()
            except NoSuchElementException:
                break

        return friend_entities

    def login(self, username, password):
        # load the login page
        self.driver.get(FbCrawler.login_url)

        # enter username and password, then click the login button
        self.driver.find_element_by_id('m_login_email').send_keys(username)
        self.driver.find_element_by_name('pass').send_keys(password)
        self.driver.find_element_by_name('login').click()
        self.driver.find_element_by_class_name('bj').click()

    def next_article_page(self):
        next_button_texts = [
            'Xem tin khác',
            'Hiển thị thêm'
        ]

        for next_button_text in next_button_texts:
            try:
                self.driver.find_element_by_xpath(f"//*[contains(text(), '{next_button_text}')]").click()
                return True
            except NoSuchElementException:
                continue

        raise LastPageException

    def get_articles(self, page_url=None) -> List[FbArticleEntity]:

        """
        :param page_url: page_url can be user url or fan page url
        :return: a list of article entities
        """

        if page_url is None:
            raise Exception('Empty user URL')

        if page_url is None:
            raise Exception('Invalid URL')

        # load user's timeline
        self.driver.get(page_url)
        current_page = 0
        article_entities = []

        while True:
            # get first layer article elements, which can be posted by users themselves or others
            article_elements = self.driver.find_elements_by_xpath("//div[@role='article' and @data-ft]")
            print(len(article_elements))

            # extract information from article elements
            for article_element in article_elements:
                content = None
                full_article_url = None
                time = None
                emotion = None

                # get privacy
                privacy = article_element\
                    .find_element_by_xpath('''.//div[@data-ft='{"tn":"*W"}']/div[1]/span[last()]''')\
                    .get_attribute('innerText')

                # get time
                try:
                    time = article_element \
                        .find_element_by_xpath('''.//div[@data-ft='{"tn":"*W"}']/div[1]/abbr''') \
                        .get_attribute('innerText')

                    time = self.extract_time(time)
                except NoSuchElementException:
                    pass

                # get short content
                try:
                    content_element = article_element.find_element_by_xpath('''.//div[@data-ft='{"tn":"*s"}']''')
                    content = content_element.get_attribute('innerText')
                except NoSuchElementException:
                    pass

                # get full content
                full_article_url = article_element.find_element_by_xpath(f".//*[contains(text(), 'Toàn bộ tin')]")\
                    .get_attribute('href')

                content, emotion = self.get_full_article(full_article_url)

                article_entity = FbArticleEntity(content=content,
                                                 page_url=page_url,
                                                 full_article_url=full_article_url,
                                                 privacy=privacy,
                                                 time=time,
                                                 emotion=emotion)

                article_entities.append(article_entity)
                print(article_entity.content, article_entity.emotion)
                print('--------------------------------------------------------------------------------------------')

            # next page
            try:
                self.next_article_page()
                current_page = current_page + 1
            except LastPageException:
                break

        return article_entities

    @staticmethod
    def extract_time(string):

        now = datetime.now()
        string = string.lower()
        result = None

        try:
            # 28 tháng 6 lúc 23:04
            match = re.match('^([0-9]{,2}) tháng ([0-9]{,2}) lúc ([0-9]{,2}):([0-9]{,2})$', string)
            if match:
                groups = [int(match) for match in match.groups()]
                result = datetime(day=groups[0], month=groups[1], year=now.year, hour=groups[2], minute=groups[3])
                raise ReturnException

            # 31 tháng 8, 2015
            match = re.match('^([0-9]{,2}) tháng ([0-9]{,2}), ([0-9]{4})$', string)
            if match:
                groups = [int(match) for match in match.groups()]
                result = datetime(day=groups[0], month=groups[1], year=groups[2])
                raise ReturnException

            # 1 tháng 1, 2018 lúc 12:35
            match = re.match('^([0-9]{,2}) tháng ([0-9]{,2}), ([0-9]{4}) lúc ([0-9]{,2}):([0-9]{,2})$', string)
            if match:
                groups = [int(match) for match in match.groups()]
                result = datetime(day=groups[0], month=groups[1], year=groups[2], hour=groups[3], minute=groups[4])
                raise ReturnException

            # 45 phút
            match = re.match('^([0-9]+) phút$', string)
            if match:
                groups = [int(match) for match in match.groups()]
                result = now - timedelta(minutes=groups[0])
                raise ReturnException

            # 2 giờ
            match = re.match('^([0-9]+) giờ$', string)
            if match:
                groups = [int(match) for match in match.groups()]
                result = now - timedelta(hours=groups[0])
                raise ReturnException

            # 2016
            match = re.match('^([0-9]{4})$', string)
            if match:
                groups = [int(match) for match in match.groups()]
                result = datetime(year=groups[0], month=1, day=1)
                raise ReturnException

            # Hôm qua lúc 00:00
            match = re.match('^hôm qua lúc ([0-9]{,2}):([0-9]{,2})$', string)
            if match:
                groups = [int(match) for match in match.groups()]
                result = now - timedelta(days=1)
                result = result.replace(hour=groups[0], minute=groups[1])
                raise ReturnException

            # 28 tháng 6
            match = re.match('^([0-9]{,2}) tháng ([0-9]{,2})$', string)
            if match:
                groups = [int(match) for match in match.groups()]
                result = datetime(day=groups[0], month=groups[1], year=now.year)
                raise ReturnException

            # Tháng 5 năm 2015
            match = re.match('^tháng ([0-9]{,2}) năm ([0-9]{4})$', string)
            if match:
                groups = [int(match) for match in match.groups()]
                result = datetime(month=groups[0], year=groups[1], day=1)
                raise ReturnException

        except ReturnException:
            return result.replace(second=0, microsecond=0)
