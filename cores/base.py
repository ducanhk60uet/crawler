from abc import ABC, abstractmethod


class BaseCrawler(ABC):

    @abstractmethod
    def login(self, *args):
        pass

    @abstractmethod
    def get_articles(self, url=None):
        pass

    def __enter__(self):
        return self

    @abstractmethod
    def __exit__(self, exc_type, exc_value, traceback):
        pass


